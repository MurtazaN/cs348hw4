/*To calculate how much soil and fill required in a garden
 * @author - Murtaza N
 */
package com.CS242;

import java.util.Scanner;

public class Garden {
    private static final double CUBIC_FEET_PER_SQUARE_YARD = 27;
    private static double radius;
    private static double circleBedArea;
    private static double totalBedArea;
    private static double volOfCircle;
    private static double volOfSemiCircle;
    private static double soilArea;
    private static double fillArea;
    private static double plantsForCircle;
    private static double plantsForSemiCircle;
    private static double totalPlants;

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Calculate Garden requirements");
        System.out.print("\nEnter Lenght of side of garden (feet):");
        double sideLength = scan.nextDouble();
        System.out.print("Enter space between plants (feet):");
        double spacing = scan.nextDouble();
        System.out.print("Enter depth of garden soil (feet):");
        double soilDepth = scan.nextDouble();
        System.out.print("Enter depth of fill (feet):");
        double fillDepth = scan.nextDouble();

        setRadius(sideLength);
        setCircleBedArea();
        setTotalBedArea(sideLength,soilDepth);
        setVolOfCircle(soilDepth);
        setVolOfSemiCircle();
        setSoilArea();
        setFillArea();
        setPlantsForCircle(spacing);
        setPlantsForSemiCircle();
        setTotalPlantsGarden();

        System.out.println("\nRequirements");
        System.out.println("\nPlants in each semicircle garden: " + getPlantsForSemiCircle());
        System.out.println("Plants in circle garden: " + getPlantsForCircle());
        System.out.println("Total plants in garden: " + getTotalPlantsGarden());
        System.out.println("Soil for each semicircle garden: " + getVolOfSemiCircle() + " cubic yards");
        System.out.println("Soil for circle garden: " + getVolOfCircle() + " cubic yards");
        System.out.println("Total Soil for garden is: " + getSoilArea() + " cubic yards");
        System.out.println("Total fill for garden is: " + getFillArea() + " cubic yards");
    }

    /**
     * Calculates radius of garden
     * @param sideLength - side lenght of garden
     */
    public static void setRadius(double sideLength) {
        radius = sideLength / 4;
    }

    /**
     * @return radius of garden
     */
    public static double getRadius() {
        return radius;
    }

    /**
     * Calculates Area of the circle flower bed
     */
    public static void setCircleBedArea(){
        circleBedArea = Math.PI * getRadius() * getRadius();
    }

    /**
     * @return Area of the circle flower bed
     */
    public static double getCircleBedArea() {
        return circleBedArea;
    }

    /**
     * Calculates the bed area of the garden
     * @param sideLength - side lenght of garden
     * @param soilDepth - Depth of Garden
     */
    public static void setTotalBedArea(double sideLength, double soilDepth) {
        totalBedArea = sideLength * sideLength * soilDepth;
    }

    /**
     * @return bed area of garden
     */
    public static double getTotalBedArea() {
        return totalBedArea;
    }

    /**
     * Calculates Volume of the garden
     * @param soilDepth - Depth of Garden
     */
    public static void setVolOfCircle(double soilDepth) {
        volOfCircle = (Math.PI * getRadius() * getRadius() * soilDepth) / CUBIC_FEET_PER_SQUARE_YARD;
    }

    /**
     * @return Volume of the garden
     */
    public static double getVolOfCircle() {
        return volOfCircle;
    }

    /**
     * Calculates Volume of half of the garden
     */
    public static void setVolOfSemiCircle() {
        volOfSemiCircle = getVolOfCircle() / 2.0;
    }

    /**
     * @return Volume of half of the garden
     */
    public static double getVolOfSemiCircle() {
        return volOfSemiCircle;
    }

    /**
     * Calculates soil to fill the entire garden
     */
    public static void setSoilArea() {
        soilArea = (4 * getVolOfSemiCircle()) + getVolOfCircle();
    }

    /**
     * @return soil to fill the entire garden
     */
    public static double getSoilArea() {
        return soilArea;
    }

    /**
     * Calculates amount of soil needed for the garden
     */
    public static void setFillArea() {
        fillArea = (getTotalBedArea()/ CUBIC_FEET_PER_SQUARE_YARD) - getSoilArea();
    }

    /**
     * @return amount of soil needed for the garden
     */
    public static double getFillArea() {
        return fillArea;
    }

    /**
     * Calculates plants that can be in circle flower bed
     * @param spacing - space between plants
     */
    public static void setPlantsForCircle(double spacing) {
        double plants = getCircleBedArea() / (spacing * spacing);
        plantsForCircle = (int)plants;
    }

    /**
     * @return plants that can be in circle flower bed
     */
    public static double getPlantsForCircle() {
        return plantsForCircle;
    }

    /**
     * Calculates plants that can be in semicircle flower bed
     */
    public static void setPlantsForSemiCircle() {
        plantsForSemiCircle = getPlantsForCircle() / 2.0;
    }

    /**
     * @return plants that can be in semicircle flower bed
     */
    public static double getPlantsForSemiCircle() {
        return plantsForSemiCircle;
    }

    /**
     * Calculates plants that can be in the entire garden
     */
    public static void setTotalPlantsGarden() {
        totalPlants = getPlantsForCircle() + (getPlantsForSemiCircle() * 4);
    }

    /**
     * @return plants that can be in the entire garden
     */
    public static double getTotalPlantsGarden() {
        return totalPlants;
    }

}
